package com.example.jtwdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JtwDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(JtwDemoApplication.class, args);
	}

}
